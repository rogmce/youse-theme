<!DOCTYPE html>
<html style="margin-top:0!important;">
<head>
		<title><?php bloginfo('name') ?>| <?php bloginfo('description') ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>
<body>
    <nav class="menu"> 
		<div class="fixLogoDiv">
			<a href="<?php echo(get_site_url()); ?>">
				<?php include("images/logoYouse.svg"); ?>
			</a>
		</div>
		<h1 class="nav__logo">Seguro Auto, Residencial e de Vida | Youse Seguro Online</h1>
		<ul class="active">
		<?php 
			$menuCat = wp_get_nav_menu_items('Categories');
			 foreach($menuCat as $menuCatLI) { 
					$title = $menuCatLI->title;
					$menuCatLIArray = explode(" ",$title);
				?>
				 <li class="catStyle"><a class="catStyleColor" href="<?php echo $menuCatLI->url ?>"><?php 
				 	for($i=0;$i<count($menuCatLIArray);$i++) {
						if($menuCatLIArray[$i] == end($menuCatLIArray)) {
							echo "<strong>".$menuCatLIArray[$i]."</strong>";
						} else {
							echo $menuCatLIArray[$i]." ";
						}
					 }
				 ?></a></li>
		<?php } 
			$menuTop = wp_get_nav_menu_items('Top Menu');
			foreach($menuTop as $menuTopLI) { ?>
				<li><a href="<?php echo $menuTopLI->url ?>"><?php echo $menuTopLI->title ?></a></li>
			<?php } ?>
	    </ul>
        <a class="toggle-nav" href="#">&#9776;</a>
    </nav>