$(document).ready(function() {
    carousel = (function(){
        var box = document.querySelector('.carouselbox');
        var next = box.querySelector('.next');
        var prev = box.querySelector('.prev');
        var items = box.querySelectorAll('.contentCarousel li');
        var counter = 0;
        var amount = items.length;
        var current = items[0];
        box.classList.add('activeCarousel');
        function navigate(direction) {
          current.classList.remove('currentCarousel');
          counter = counter + direction;
          if (direction === -1 && 
              counter < 0) { 
            counter = amount - 1; 
          }
          if (direction === 1 && 
              !items[counter]) { 
            counter = 0;
          }
          current = items[counter];
          current.classList.add('currentCarousel');
        }
        next.addEventListener('click', function(ev) {
          navigate(1);
        });
        prev.addEventListener('click', function(ev) {
          navigate(-1);
        });
        navigate(0);
      })();
});