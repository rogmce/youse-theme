        <footer>
            <div class="yousemon">
			 	<?php include("images/Yousemon.svg"); ?>
			</div>
            <div class="row">
                <div class="col-6">
                    <span style="float:left"><?php include("images/YOUSE-neg.svg"); ?></span>
                    <p style="float:left;margin-top:3px;">Caixa Seguradora</p>
                </div>
                <div class="col-6 socialIcons">
                    <a href="https://youse.app.link/twitter"><i class="fab fa-twitter"></i></a>
                    <a href="https://youse.app.link/instagram"><i class="fab fa-instagram"></i></a>
                    <a href="https://youse.app.link/linkedin"><i class="fab fa-linkedin"></i></a>
                    <a href="https://youse.app.link/facebook"><i class="fab fa-facebook-f"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>Youse é uma plataforma de venda de seguros online da Caixa Seguradora. Os produtos comercializados são garantidos pela Caixa Seguradora S.A., inscrita no CNPJ nº 34.020.354/0001-10, sediada no SHN, Quadra 01, Conjunto A, Bloco E, CEP 70701 050 – Brasília DF, sendo intermediados pela Wiz Soluções e Corretagem de Seguros S.A. O registro deste plano na SUSEP não implica, por parte da Autarquia, incentivo ou recomendação à sua comercialização. Processo SUSEP Residencial nº 15.414.900040/2016-34. As condições contratuais/regulamento deste produto protocolizadas pela sociedade/entidade junto a SUSEP poderão ser consultadas no endereço eletrônico www.susep.gov.br, de acordo com o número de processo constante da apólice/proposta.</p>
                    <p>© 2018 Youse ‐ Todos os direitos reservados.</p>
                    <p>Política de Privacidade</p>
                    <p>Entre em contato com a gente pelos números:</p>
                    <p>Vendas: 0800 730 9902 | SAC: 0800 730 9901 | Ouvidoria: 0800 702 4240 | Surdo/Mudo: 0800 730 9904 | Atendimento ao Público Susep: 0800 021 8484</p>
                <div class="col-12">
            </div>
        </footer>
    </body>
</html>
