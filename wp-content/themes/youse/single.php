<?php get_header(); ?>
<?php $postID = get_the_ID(); $authorID = get_post_field('post_author', $postID); $authorName = get_the_author_meta('display_name',$authorID); ?>
    <div class="row purpleDiv">
      <div class="col-12 purpleDivContentPost">
			     <h1 class="postTitle"><strong><?php echo get_the_title(); ?></strong></h1>
			     <p class="callToActionText"><?php echo get_the_excerpt(); ?></p>
          <?php $category = get_the_category($postID); ?>
          <a class="buttonPost" href="<?php $cat = $category[1];echo  get_category_link($cat->cat_ID) ?>" class="knowAbout"><?php echo $category[1]->name?></a>
      </div>
	</div>
	<div  class="row catMenuOrange">
		<div style="padding:0px!important;" class="col-12">
			<p style="color:white;font-size:20px;float-center;"><?php echo get_the_date(); ?> - <?php echo $authorName; ?></p>
		</div>
	</div>
    <div style="margin-top:0;" id="primary" class="site-content">
        <div id="content" role="main">
		<?php
			echo apply_filters('the_content', get_post_field('post_content', $postID));
		?>
		</div>
	</div>
	<style>
		.yousemon {
			display:none;
		}
	</style>
<?php get_footer(); ?>
