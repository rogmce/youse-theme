<?php get_header(); ?>
    <div class="row purpleDiv">
        <div class="col-6 purpleDivLeftContent">
            <p class="callToActionTitle">[KV]</p><br><p class="callToActionText">Agora o seu Seguro te dá <strong>descontos e benefícios</strong> pra vc ousar mais por aí.</p>
            <a href="https://www.youse.com.br/seguro-auto/friends/" class="knowAbout">CONHEÇA</a>
        </div>
        <div class="col-6 purpleDivRightContent"><iframe width="100%" height="260" src="https://www.youtube.com/embed/_0ujPrd1U1M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
	</div>
	<div class="row catMenuOrange">
		<div class="col-12">
		<ul class="fixLastCatStyleOrangeBorder">
		<?php
			$menuCat = wp_get_nav_menu_items('Categories');
			 foreach($menuCat as $menuCatLI) {
					$title = $menuCatLI->title;
					$menuCatLIArray = explode(" ",$title);
				?>
				 <li class="catStyleOrange"><a class="catStyleColorOrange" href="<?php echo $menuCatLI->url ?>"><?php
				 	for($i=0;$i<count($menuCatLIArray);$i++) {
						if($menuCatLIArray[$i] == end($menuCatLIArray)) {
							echo "<strong>".$menuCatLIArray[$i]."</strong>";
						} else {
							echo $menuCatLIArray[$i]." ";
						}
					 }
				 ?></a></li>
		<?php } ?>
	    </ul>
		</div>
	</div>
    <div id="primary" class="site-content">
        <div id="content" role="main">
          <?php
              $colCounter = 0;
              $argsLoop = array (
                  'cat' => $category->cat_ID
              );
              $postsLoop = new WP_Query($argsLoop);
              while ( $postsLoop->have_posts() ) : $postsLoop->the_post();
              switch($colCounter) {
              case 0:
                  echo "<div class='row'>
            <div class='col-6 bottomBorder'><a href='".get_permalink()."'>";
                    the_post_thumbnail(array('500','225'));
                      echo "</a>";
                  break;

              case 3:
                  echo "<div class='row'>
            <div class='col-3 bottomBorder'><a href='".get_permalink()."'>";
            the_post_thumbnail(array('224','224'));
            echo "</a>";
            break;

              case 5:
        echo "<div class='col-6 bottomBorder'><a href='".get_permalink()."'>";
          the_post_thumbnail(array('500','225'));
          echo "</a>";
          break;

              default:
        echo "<div class='col-3 bottomBorder'><a href='".get_permalink()."'>";
        the_post_thumbnail(array('224','224'));
        echo "</a>";
        break;
          ?>
          <?php } ?>
          <?php $categories = get_the_category(); ?>
                      <h3 class="parentCat"><a class="parentCat" href="<?php echo get_category_link($categories[1]->cat_ID) ?>"><?php echo $categories[1]->name ?></a></h3>
                      <h3 class="childCat"><a class="childCat" href="<?php echo get_category_link($categories[0]->cat_ID) ?>"><?php echo $categories[0]->name ?></a></h3>
                      <h2 class="fixHeadTwo"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          <?php
              switch($colCounter) {
                  case 2:
                      echo "</div><!--col-->
                              </div><!-- row -->";break;

                  case 5:
                      echo "</div><!--col-->
                              </div><!-- row -->";break;

                  default:
                      echo "</div><!--col-->";break;
              }
          ?>
          <?php
              $colCounter++;
              if($colCounter == 6) {
                  $colCounter = 0;
              }
              endwhile; // end of the loop.
              if($colCounter < 5)  {
                  echo "</div><!--row-->";
              }
              if (!$postsLoop->have_posts()) {
                  echo "<p>Ainda não temos nada nessa sessão, mas estamos preparando algo especial para você.</p><p> Fique de olho ;)</p>";
              };
          wp_reset_postdata();
          ?>
        </div><!-- #content -->
    </div><!-- #primary -->
    <div id="second" class="site-content">
        <?php
            $postTypeNamePartners = get_post_type_object('partners')->label;
            $postTypeNamePartnersArray = explode(" ",$postTypeNamePartners);
        ?>
            <h4 class="partnersTitle"><?php
                for($i=0;$i<count($postTypeNamePartnersArray);$i++) {
                    if($postTypeNamePartnersArray[$i] == $postTypeNamePartnersArray[0]) {
                        echo $postTypeNamePartnersArray[$i]." ";
                    } else {
                        echo "<strong>".$postTypeNamePartnersArray[$i]."</strong> ";
                    }
                }
          ?></h4>
        <div id="content" role="main">
				<div class="row">
					<?php
					$argsSpecialPartners = array(
						'post_type'=>'partners',
						'meta_key'=>'meta-box-linkPartner-dropdown',
						'meta_value'=>'SIM',
					);
					$loopSpecialPartners = new WP_Query($argsSpecialPartners);
					while ( $loopSpecialPartners->have_posts() ) : $loopSpecialPartners->the_post(); ?>
					<div class="col-6 fixSpecialPartners">
						<?php the_post_thumbnail(); ?>
					</div>
				<?php endwhile; // end of the partners loop.
					wp_reset_postdata();
				?>
				</div>
				<div id="dissapearWhenSmall">
				<?php
				$argsPartners = array(
					'post_type'=>'partners',
					'meta_key'=>'meta-box-linkPartner-dropdown',
					'meta_value'=>'NÃO',
          'posts_per_page' => '12'
					);
					$loopPartners = new WP_Query($argsPartners);
					$countPartners = 0;
					while ( $loopPartners->have_posts() ) : $loopPartners->the_post();
					switch($countPartners) {
						case 0:
							echo "<div class='row'>
										<div class='col-2 fullBorder'>";break;
						case 6:
							echo "<div class='row'>
										<div class='col-2 fullBorder'>";break;
						default:
							echo "<div class='col-2 fullBorder'>";break;
					} ?>
						<?php the_post_thumbnail(); ?>
					<?php
					switch($countPartners) {
						case 5:
							echo "</div><!--col-->
										</div><!--row-->";break;

						case 12:
							echo "</div><!--col-->
										</div><!--row-->";break;

						default:
							echo "</div><!--col-->";break;
					}
					$countPartners++;
					endwhile; // end of the partners loop.
					wp_reset_postdata();
				?>
				</div>
			</div>
        </div><!--Content-->
	</div><!--Second-->
	<div class="carouselWrapper">
	<div class="carouselbox activeCarousel">
					<div class="buttons">
						<button class="prev">
						<i class="fas fa-arrow-left"></i><span class="offscreen">Previous</span>
						</button>
						<button class="next">
							<span class="offscreen">Next</span><i class="fas fa-arrow-right"></i>
						</button>
					</div>
					<ol class="contentCarousel">
					<?php
					$argsPartners = array(
						'post_type'=>'partners',
						'meta_key'=>'meta-box-linkPartner-dropdown',
						'meta_value'=>'NÃO',
						'post_per_page'=>'12'
						);
						$loopPartners = new WP_Query($argsPartners);
						$counterSlider = 0;
						while ( $loopPartners->have_posts() ) : $loopPartners->the_post();
					?>
						<li <?php if($counterSlider == 0){ echo ("class='current'");} ?>>
							<?php the_post_thumbnail(); ?>
						</li>
					<?php
						$counterSlider++;
					endwhile;
						wp_reset_postdata();
					?>
					</ol><!--End Carousel List-->
					</div><!--Close Carousel Box -->
				</div><!-- Close Carousel Wraper -->
		<div id="third" class="site-content">
        	<div id="content" role="main">
			<div class="row">
				<div class="col-12 fix_yousemon">
				<h4 class="sacTitle"><?php echo get_post_type_object('sac')->label ?></h4>
				<p class="sacDescription"><?php echo get_post_type_object('sac')->description ?></p>
            <?php
                $argsSac = array (
                    'post_type'=>'sac'
                );
                $postsSac = new WP_Query($argsSac); ?>
				<div id="accordion">
				<?php while ( $postsSac->have_posts() ) : $postsSac->the_post();
				?>
                    <h2 class="accordionOption"><i class="fas fa-plus">&nbsp;</i><?php the_title(); ?></h2>
					<div>
						<?php the_excerpt() ?>
					</div>
                <?php endwhile; // end of the loop.
            wp_reset_postdata();
			?>
			 </div>
			</div>

   </div>
			</div><!-- #content -->
			</div><!-- #third -->
<?php get_footer(); ?>
