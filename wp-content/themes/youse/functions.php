<?php
/*Enqueue scripts and styles*/
function youse_enqueue() {
    wp_enqueue_style( 'style.css', get_stylesheet_uri() );
    wp_enqueue_style( 'font_awesome', 'https://use.fontawesome.com/releases/v5.7.1/css/all.css', array(), 5.1, false);
    wp_enqueue_script( 'jQuery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), 3.1, false);
    wp_enqueue_script( 'toggleScript', get_template_directory_uri() . '/js/toggleScript.js', array(), 1.0, false);
    wp_enqueue_script( 'carouselScript', get_template_directory_uri() . '/js/carousel.js', array(), 1.0, false);
    wp_enqueue_script( 'accordionScript', get_template_directory_uri() . '/js/accordion.js', array(), 1.0, false);
    wp_enqueue_script( 'fixPicScript', get_template_directory_uri() . '/js/fixPics.js', array(), 1.0, false);
    wp_enqueue_script( 'jQueryUI', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array(), 1.12, false);
}
add_action( 'wp_enqueue_scripts', 'youse_enqueue' );

/*Adding post thumbnails for all posts*/
add_theme_support( 'post-thumbnails' );

/*Creating post_type for partners*/
function partners_post_type() {
    $labels = array(
        'name'                => _x( 'Parceiros em destaque', 'Post Type General Name', 'youse' ),
        'singular_name'       => _x( 'Parceiro', 'Post Type Singular Name', 'youse' ),
        'menu_name'           => __( 'Parceiros', 'youse' ),
        'parent_item_colon'   => __( 'Parceiros', 'youse' ),
        'all_items'           => __( 'Todos os parceiros', 'youse' ),
        'view_item'           => __( 'Ver', 'youse' ),
        'add_new_item'        => __( 'Adicionar novo parceiro', 'youse' ),
        'add_new'             => __( 'Adicionar novo', 'youse' ),
        'edit_item'           => __( 'Editar parceiro', 'youse' ),
        'update_item'         => __( 'Atualizar parceiro', 'youse' ),
        'search_items'        => __( 'Procurar parceiro', 'youse' ),
        'not_found'           => __( 'Não encontrado', 'youse' ),
        'not_found_in_trash'  => __( 'Não encontrado no lixo', 'youse' ),
    );
    $args = array(
        'label'               => __( 'Parceiros em destaque', 'youse' ),
        'description'         => __( 'Our best partners', 'youse' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'thumbnail', 'revisions'),
        'hierarchical'        => false,
        'public'              => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'menu_icon'           => 'dashicons-smiley'
    );
    register_post_type( 'partners', $args );
}
add_action( 'init', 'partners_post_type', 0 );

/*Add metabox content for the partner link*/
function add_meta_box_linkPartner($object) {
    wp_nonce_field(basename(__FILE__), "meta-box-linkPartner-nonce");

    ?>
        <div>
            <label for="meta-box-linkPartner-text">Link da Categoria do Parceiro</label>
            <input name="meta-box-linkPartner-text" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-linkPartner-text", true); ?>">
            <br>
            <label for="meta-box-linkPartner-dropdown">Parceiro Especial</label>
            <select name="meta-box-linkPartner-dropdown">
            <?php
                $option_values = array("SIM", "NÃO");

                foreach($option_values as $key => $value)
                {
                    if($value == get_post_meta($object->ID, "meta-box-linkPartner-dropdown", true))
                    {
                        ?>
                            <option selected><?php echo $value; ?></option>
                        <?php
                    }
                    else
                    {
                        ?>
                            <option><?php echo $value; ?></option>
                        <?php
                    }
                }
            ?>
            </select>
    <?php
}

/*Add metabox linkPartner*/
function add_linkPartner_meta_box() {
    add_meta_box("LinkPartner", "Infos Parceiro", "add_meta_box_linkPartner", "partners", "normal", "high", null);
}
add_action("add_meta_boxes", "add_linkPartner_meta_box");

/*Change the enter title for partner post_type*/
function partnerLink_enter_title( $input ) {
    if ( 'partners' === get_post_type() ) {
        return __( 'Nome do Parceiro', 'youse' );
    }
    return $input;
}
add_filter( 'enter_title_here', 'partnerLink_enter_title' );

/*Save the partners content*/
function save_linkPartners_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-linkPartner-nonce"]) || !wp_verify_nonce($_POST["meta-box-linkPartner-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "partners";
    if($slug != $post->post_type)
        return $post_id;

    $meta_box_text_value = "";
    $meta_box_dropdown_value = "";

    if(isset($_POST["meta-box-linkPartner-text"]))
    {
        $meta_box_text_value = $_POST["meta-box-linkPartner-text"];
    }
    update_post_meta($post_id, "meta-box-linkPartner-text", $meta_box_text_value);

    if(isset($_POST["meta-box-linkPartner-dropdown"]))
    {
        $meta_box_dropdown_value = $_POST["meta-box-linkPartner-dropdown"];
    }
    update_post_meta($post_id, "meta-box-linkPartner-dropdown", $meta_box_dropdown_value);
}
add_action("save_post", "save_linkPartners_meta_box", 10, 3);

/*Creating SAC Custom Post Type*/
function sac_post_type() {
    $labels = array(
        'name'                => _x( 'Tem dúvidas?', 'Post Type General Name', 'youse' ),
        'singular_name'       => _x( 'Dúvida', 'Post Type Singular Name', 'youse' ),
        'menu_name'           => __( 'SAC', 'youse' ),
        'parent_item_colon'   => __( 'Dúvidas', 'youse' ),
        'all_items'           => __( 'Todas as dúvidas', 'youse' ),
        'view_item'           => __( 'Ver', 'youse' ),
        'add_new_item'        => __( 'Adicionar nova dúvida', 'youse' ),
        'add_new'             => __( 'Adicionar nova', 'youse' ),
        'edit_item'           => __( 'Editar dúvida', 'youse' ),
        'update_item'         => __( 'Atualizar dúvida', 'youse' ),
        'search_items'        => __( 'Procurar dúvida', 'youse' ),
        'not_found'           => __( 'Não encontrada', 'youse' ),
        'not_found_in_trash'  => __( 'Não encontrada no lixo', 'youse' ),
    );
    $args = array(
        'label'               => __( 'sac', 'youse' ),
        'description'         => __( 'A gente te explica tudo direitinho.', 'youse' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'excerpt', 'revisions'),
        'hierarchical'        => false,
        'public'              => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'menu_icon'           => 'dashicons-editor-help'
    );
    register_post_type( 'sac', $args );
}
add_action( 'init', 'sac_post_type', 0 );

/*Change the enter title for sac post_type*/
function sac_enter_title( $input ) {
    if ( 'sac' === get_post_type() ) {
        return __( 'Digite aqui a dúvida', 'youse' );
    }
    return $input;
}
add_filter( 'enter_title_here', 'sac_enter_title' );
